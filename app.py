from flask import Flask, render_template, jsonify, request
from pymongo import MongoClient
from pymongo.collection import Collection
import os, json
from flask_bootstrap import Bootstrap
from bson import ObjectId
#Configuração do APP
app = Flask('__name__')
Bootstrap(app)
string_config = "mongodb+srv://admin:V12cI70z36gTOw3H@flask-academictrack-v0-z0mpa.mongodb.net/test?retryWrites=true&w=majority"

# Conectando com o banco de dados
client = MongoClient(string_config)
banco = client.get_database('academic-tracker')
courses = Collection(banco, 'courses')

#Acessando o arquivo e escrevendo o dicionário para a postagem
data = json.loads(open('database.json','r').read())
courses_list = []

for course in data['courses']:
    course_dict = {'title':'','description':'','URL':'','provider':'','keywords':''}
    course_dict['title'] = data['courses'][course]['title']
    course_dict['description'] = data['courses'][course]['description']
    course_dict['URL']= data['courses'][course]['URL']
    course_dict['provider']= data['courses'][course]['provider']
    course_dict['keywords'] = data['courses'][course]['keywords']
    courses_list.append(course_dict)

#Rota de 'POST'
@app.route('/post/')
def index():
    for course in courses_list:
        banco.database.courses.insert_one({'title':course['title'] , 'description':course['description'] , 'URL':course['URL'] , 'provider':course['provider'] , 'keywords':course['keywords']})
    return 'Posting was success!'

#Rota de GET
@app.route('/courses/')
def get():

    provider = request.args.get('provider')
    title = request.args.get('title')
    keywords = request.args.get('keywords')
    _id = request.args.get('id')

    params = {'provider':provider,'title':title,'keywords':keywords,'_id':_id}

    if _id:
        params['id'] = {'$regex':_id,'$options':'i'}
    if provider:
        params['provider'] = {'$regex':provider,'$options':'i'}
    if title:
        params['title'] = {'$regex':title,'$options':'i'}
    if keywords:
        params['keywords'] = {'$regex':keywords,'$options':'i'}

    query = {key:value for key, value in params.items() if value}
    courses = banco.database.courses.find(query,{'_id':0})

    return jsonify(list(courses))

if __name__== '__main__':
    app.run(debug=True)
